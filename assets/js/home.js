let post;
fetch("./posts.json")
.then(response => {
   return response.json();
})
.then(function(data){
   post = data.posts;
   addButtons(post.length);
   for(var x="0";x<post.length&&x<4;x++){
      addobject(post[x]);
   } 
});

load=function(n){
   document.getElementById("articlecontainer").innerHTML="";
   for(var x=n*4;x<post.length&&x<n*4+4;x++){
      addobject(post[x]);
   }}

addButtons=function(num){
   let n=num/4;
   n++;
   let date_container=document.getElementById("buttons");
   date_container.innerHTML="";
   for(let pno=0;pno<n-1;pno++){
      let button=document.createElement("button");
      button.setAttribute("id",pno);
      button.setAttribute("class","pages");
      button.setAttribute("onClick","load(this.id)");
      button.innerHTML=pno+1;
      date_container.appendChild(button);
   }
}
addobject=function(obj){
   //creating article
   let article=document.createElement('div');
   article.setAttribute("class","article");
   document.getElementById('articlecontainer').appendChild(article);

   let container=document.createElement("div");
   container.setAttribute("class","container");
   article.appendChild(container);

   //img
   let img=document.createElement('img');
   img.src="./"+obj.img;
   container.appendChild(img);

   //text on image
   let imagebelow=document.createElement("div");
   imagebelow.setAttribute("class","imagebelow");
   imagebelow.innerHTML=obj.category;
   container.appendChild(imagebelow);

   //title
   let heading=document.createElement("h2");
   heading.innerHTML=obj.title;
   article.appendChild(heading);

   //date
   let date =document.createElement("span");
   date.setAttribute("class","date");
   date.innerHTML=obj.datetime+"  /";
   article.appendChild(date);
   x=new Date(obj.datetime*1000);
   AmPm="";
   if(x.getHours>12){
      hour=x.getHours%12;
      AmPm="pm"
   }else{
      hour=x.getHours();
      AmPm="am";
   }
   var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
   date.innerHTML=(x.getDate()+" "
   +months[x.getMonth()]+","+
   x.getFullYear()+" "
   +hour+":"
   +x.getMinutes()
   +AmPm+" "
   );

   //author
   let author =document.createElement("span");
   author.setAttribute("class","author");
   author.innerHTML="By: "+obj.author;
   article.appendChild(author);
   
   let comments =document.createElement("span");
   comments.setAttribute("class","comment");
   comments.innerHTML=obj.comment_count+"Comments";
   article.appendChild(comments);

   let summary =document.createElement("p");
   summary.setAttribute("class","summary");
   summary.innerHTML=obj.desc;
   article.appendChild(summary);
   

}


let slides=[];

fetch("./slides.json")
.then(response => {
   return response.json();
})
.then(function(data){
   slides=data;
});

setTimeout(function(){ }, 100);

let index=0;
$("#right-arrow").click(function(){
   index++;
   if (index>slides.length) {
      index=0
   }
   changeslider(index);
})
$("#left-arrow").click(function(){
   index--;
   if (index==-1) {
      index=slides.length;
   }
   changeslider(index);
})
changeslider=function(index){
   document.getElementById("sliderimg").setAttribute("src","./"+slides[index].img);
   document.getElementById("slidertitle1").innerHTML=slides[index].title;
   document.getElementById("sliderdesc").innerHTML=slides[index].desc;
}
let searchobject=document.getElementById("search");
let searcharray=[];
let temp=true;
//search and display
searchobject.onchange=function(e){
   searcharray=[];
   document.getElementById("articlecontainer").innerHTML="";
   for(let x=0;x<post.length;x++){
      let tmp=post[x];
      let objstring=tmp.title+" "+tmp.desc+" "+tmp.category+" ";
      objstring=objstring.toLowerCase();
      searchstring=e.target.value.toLowerCase();
      if(objstring.search(searchstring)>=0){
         searcharray.push(post[x]);
      }
   }
   for(let x=0;x<searcharray.length && x<4;x++){
      addobject(searcharray[x])
   }
   searchstring==""?addButtons(searcharray.length):addSearchButtons(searcharray.length);
   console.log(searcharray);
}
loadSearch=function(n){
   document.getElementById("articlecontainer").innerHTML="";
   for(var x=n*4;x<searcharray.length && x<n*4+4;x++){
      addobject(searcharray[x]);
   }
   console.log(x);
}

addSearchButtons=function(num){
   let n=num/4;
   n++;
   let date_container=document.getElementById("buttons");
   date_container.innerHTML="";
   console.log(" n = "+n);
   for(let pno=0;pno<n-1;pno++){
      let button=document.createElement("button");
      button.setAttribute("id",pno);
      button.setAttribute("onClick","loadSearch(this.id)");
      button.innerHTML=pno+1;
      date_container.appendChild(button);
   }
}